cmake_minimum_required(VERSION 2.8.9)

project (mbedtls)

add_subdirectory(include)
add_subdirectory(library)
